import health from './health';
import main from './main';

export default {
  health: health.check,
  index: main.index
};
