/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import health from './health';
import {iContext} from '../lib/context';

describe('controllers/health', () => {
  it('should modify ctx', () => {
    let ctx:any = {};
    health.check((ctx as iContext));
    assert.equal(ctx.logRequest, false, 'logRequest !== false');
    assert.equal(ctx.body, 'ok', 'body not "ok"');
  });
});
