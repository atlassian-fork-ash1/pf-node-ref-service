import {iContext} from '../lib/context';

export default {
  index: (ctx: iContext) => {
    ctx.body = 'Product Fabric NodeJS Ref Service running :)';
  }
};
