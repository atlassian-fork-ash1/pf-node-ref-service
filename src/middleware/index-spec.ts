/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import index, {MWConfig} from './index';
import Metrics from '../lib/metrics';

const metricsInstance = new Metrics(process.env['MICROS_SERVICE'] || 'directory');
describe('middleware/index', () => {
  it('return a combined middleware', () => {
    const opts:MWConfig = {
      logger: null,
      metricsInstance: metricsInstance
    };
    assert.ok(index(opts));
  });
});
