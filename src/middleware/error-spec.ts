/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import {HttpError} from './error';
import {iContext} from '../lib/context';
import MWErrorHandler from './error';

let next = async () => Promise.resolve();

describe('middleware/error', () => {
  describe('default export', () => {
    it('should return a function', () => {
      assert.ok(typeof MWErrorHandler() === 'function');
    });
  });

  describe('#HttpError', () => {
    it('should accept a message', () => {
      const errorObject = new HttpError('errormessage');
      assert.equal(errorObject.detail, 'errormessage');
    });

    it('should expose specific properties', () => {
      const errorOpts = {
        status: 404,
        title: 'no there',
        code: 'error.notfound',
        meta: {one: true}
      };
      const errorObject = new HttpError('errormessage', errorOpts);
      assert.deepEqual(errorObject.toJSON(), {
        status: 404,
        title: 'no there',
        code: 'error.notfound',
        detail: 'errormessage',
        meta: { one: true }
      });
    });
  });

  describe('middleware', () => {
    it('should not alter the context if no error is thrown', () => {
      const mw = MWErrorHandler();
      const ctx:any = {};

      return mw((ctx as iContext), next)
        .then(() => {
          assert.deepEqual(ctx, {});
        });
    });

    it('should set 500 Server Error if thrown error not a HttpError', () => {
      const mw = MWErrorHandler();
      let loggedError:any;
      const ctx:any = {
        logger: {
          error: (err: any) => {
            loggedError = err;
          }
        }
      };
      let next = async () => Promise.reject(new Error('test error'));

      return mw((ctx as iContext), next)
        .then(() => {
          assert.equal(loggedError.toString(), 'Error: test error');
          assert.equal(ctx.status, 500);
          assert.deepEqual(ctx.body, {
            status: 500,
            title: 'Internal Server Error'
          });
        });
    });

    it('should set result according to the HttpError', () => {
      const mw = MWErrorHandler();
      let loggedError:any;
      const ctx:any = {
        logger: {
          warn: (err: any) => {
            loggedError = err;
          }
        }
      };

      let next = async () => Promise.reject(new HttpError('test error', { status: 400 }));

      return mw((ctx as iContext), next)
        .then(() => {
          assert.equal(loggedError.toString(), 'HttpError');
          assert.equal(ctx.status, 400);
          assert.deepEqual(ctx.body, {
            status: 400,
            title: 'Bad Request',
            code: undefined,
            detail: 'test error',
            meta: undefined
          });
        });
    });
  });
});
