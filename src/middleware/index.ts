import * as compose from 'koa-compose';
import Metrics from '../lib/metrics';
import * as MWjson from 'koa-json';
import MWCacheHandler from './cachehandler';
import MWConfig from './config';
import MWCors from './cors';
import MWError from './error';
import MWLogger from './logger';
import MWMetrics from './metrics';
import MWTracer from './tracer';
import MWInteractors from '../middleware/interactors';
const MWetag = require('koa-etag');

export interface MWConfig {
  logger: any;
  metricsInstance: Metrics;
}

// default middleware
export default function (opts: MWConfig) {
  return compose([
    MWTracer(),
    MWConfig(),
    MWMetrics(opts.metricsInstance),
    MWLogger(opts.logger),
    MWError(),
    MWCacheHandler({ 'no-cache': true }),
    MWetag(),
    MWjson(),
    MWCors(),
    MWInteractors()
  ]);
}
