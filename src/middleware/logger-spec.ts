/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import * as bunyan from 'bunyan';
import {iContext} from '../lib/context';
import mwlogger from './logger';

const next = async () => Promise.resolve();



const delayedNext = async () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({});
    }, 25);
  });
};

describe('middleware/logger', () => {
  let headers:{[key: string]: string | string[]} = {};
  let logger:bunyan.Logger;
  let tracerName: string;

  const ctx = {
    config: {
      logger: {}
    },
    request: {
      url: 'http://testhost.com'
    },
    fresh: false,
    status: 0,
    logger: logger,
    tracer: {
      setName(path: string) {
        tracerName = path;
      },
      getTraceLog() {
        return {};
      }
    },
    set: (field: string | any, val?: string | string[]) => {
      if (val) {
        headers[field] = val;
        return;
      }
      Object.keys(field).forEach((fieldName) => {
        headers[fieldName] = field[fieldName];
      });
    },
    get: (field: string) => {
      return headers[field];
    }
  };

  beforeEach(() => {
    headers = {};
    logger = bunyan.createLogger({
      level: 'debug',
      name: 'fab-ppl-directory',
      serializers: bunyan.stdSerializers
    });
  });

  it('should attach a logger if no opts are given', () => {
    const promise = mwlogger(logger);

    return promise((ctx as iContext), next)
      .then(() => {
        assert.ok((ctx as any).logRequest, 'ctx.logRequest not set');
      });
  });

  it('should accept responseTimeLimit option', () => {
    const promise = mwlogger(logger, {responseTimeLimit:1});

    return promise((ctx as iContext), delayedNext);
  });

  it('should not log if ctx.logRequest === false', () => {
    const promise = mwlogger(logger, {responseTimeLimit:1});
    const nextNoLog = async () => {
      (ctx as any).logRequest = false;
      Promise.resolve();
    };

    return promise((ctx as iContext), nextNoLog);
  });

});
