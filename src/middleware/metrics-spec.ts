/*globals it, describe, beforeEach, afterEach */

import * as assert from 'assert';
import metricsMW from './metrics';
import Metrics from '../lib/metrics';

const next = async () => Promise.resolve();
const metricsInstance = new Metrics(process.env['MICROS_SERVICE'] || 'directory');

describe('middleware/metrics', () => {
  it('returns a combined middleware', () => {
    const mw = metricsMW(metricsInstance);
    assert.ok(mw);
  });

  it('adds metrics to the context', () => {
    const mw = metricsMW(metricsInstance);
    const ctx:any = {};
    return mw(ctx, next)
      .then(() => {
        assert.ok(ctx.metrics);
      });
  });
});
