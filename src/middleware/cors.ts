import {iContext} from '../lib/context';
const extend = require('deep-extend');

interface TMWCorsOpts {
  origin?: string;
}

export default (opts: TMWCorsOpts = { origin: '*' }) => {
  return async (ctx: iContext, next: Function) => {
    opts = extend(ctx.config.cors, opts);
    ctx.set({
    //  'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Headers': ctx.get('access-control-request-headers'),
      'Access-Control-Allow-Origin': (opts.origin === '*') ? ctx.get('origin') : opts.origin,
      'Access-Control-Allow-Methods': 'GET,HEAD,PUT,POST,DELETE',
      'Access-Control-Allow-Credentials': 'true'
    //  'Access-Control-Expose-Header': 'WWW-Authenticate', // unfortunately only available in Firefox/Gecko atm
    });
    await next();
  };
};
