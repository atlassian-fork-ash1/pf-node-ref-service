import {iContext} from '../lib/context';
import * as boom from 'boom';

interface HttpErrorOpts {
  status?: number;
  title?: string;
  code?: string;
  meta?: any;
}

export class HttpError extends Error {

  status: number;
  title: string;
  code: string;
  detail: string;
  meta: any;

  constructor(detail: string, opts?: HttpErrorOpts) {
    super();
    this.name = 'HttpError';
    opts = opts || {};
    this.status = opts.status || 500;
    const boomError:any = boom.create(this.status);
    this.title = opts.title || boomError.message;
    this.detail = detail;
    this.code = opts.code;
    this.meta = opts.meta;
  }

  toJSON() {
    return {
      status: this.status,
      title: this.title,
      code: this.code,
      detail: this.detail,
      meta: this.meta
    };
  }
}


export default function () {
  return async (ctx: iContext, next: Function) => {
    try {
      await next();
    } catch (err) {
      ctx.status = err.status || 500;
      if (err instanceof HttpError) {
        ctx.logger.warn(err);
        ctx.body = err.toJSON();
      } else {
        ctx.logger.error(err);
        ctx.body = {
          status: 500,
          title: 'Internal Server Error'
        };
      }
    }
  };
};
