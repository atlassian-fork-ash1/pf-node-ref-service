import * as crypto from 'crypto';
import {iContext} from './context';

export interface iTraceInfo {
  traceId?: string;
  spanId?: string;
  parentSpanId?: string;
  name?: string;
  recvTimestamp?: number;
  sendTimestamp?: number;
}

interface traceLogInfo {
  trace_id: string;
  span_id: string;
  parent_span_id?: string;
  type: string;
  name: string;
  recv_timestamp: number;
  send_timestamp: number;
};

export class Tracer {

  public traceInfo: iTraceInfo = {};

  constructor(traceInfo: iTraceInfo = {}, name?: string) {
    this.setTraceInfo(traceInfo);
    if (name) {
      this.setName(name);
    }
    this.timingStart();
  }

  setName(name: string) {
    if (name) {
      this.traceInfo.name = name.toLowerCase();
    }
  }

  timingStart(ts?: number) {
    this.traceInfo.recvTimestamp = ts || new Date().getTime();
  }

  timingEnd(ts?: number) {
    this.traceInfo.sendTimestamp = ts || new Date().getTime();
  }

  setTraceInfo(info: iTraceInfo) {
    const traceId = info.traceId || Tracer.createTraceId();
    this.setName(info.name);
    if (info.sendTimestamp) {
      this.traceInfo.sendTimestamp = info.sendTimestamp;
    }
    this.traceInfo.traceId = traceId;
    this.traceInfo.spanId = info.spanId || traceId;
    this.traceInfo.parentSpanId = info.parentSpanId;
  }

  readTraceInfoFromRequest(ctx: iContext) {
    this.setTraceInfo({
      traceId: ctx.get('x-b3-traceid'),
      spanId: ctx.get('x-b3-spanid'),
      parentSpanId: ctx.get('x-b3-parentspanid')
    });
  }

  writeTraceInfoToResponse(ctx: iContext) {
    ctx.set('x-b3-traceid', this.traceInfo.traceId);
    ctx.set('x-b3-spanid', this.traceInfo.spanId);
    if (this.traceInfo.parentSpanId) {
      ctx.set('x-b3-parentspanid', this.traceInfo.parentSpanId);
    }
  }

  forkTraceInfo(): iTraceInfo {
    return {
      traceId: this.traceInfo.traceId,
      spanId: Tracer.createTraceId(),
      parentSpanId: this.traceInfo.spanId
    };
  }

  forkTracer(name?: string): Tracer {
    return new Tracer(this.forkTraceInfo(), name);
  }

  getTraceLog(): traceLogInfo {
    if (!this.traceInfo.sendTimestamp) {
      this.timingEnd();
    }

    const traceInfo: traceLogInfo = {
      trace_id: this.traceInfo.traceId,
      span_id: this.traceInfo.spanId,
      type: 'server',
      name: this.traceInfo.name || 'undefined',
      recv_timestamp: this.traceInfo.recvTimestamp,
      send_timestamp: this.traceInfo.sendTimestamp
    };

    if (this.traceInfo.parentSpanId) {
      traceInfo.parent_span_id = this.traceInfo.parentSpanId;
    }

    return traceInfo;
  }

  static createTraceId() {
    return crypto.randomBytes(8).toString('hex');
  }
}
