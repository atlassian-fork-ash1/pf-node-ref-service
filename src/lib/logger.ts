import * as bunyan from 'bunyan';

export default (opts: any):bunyan.Logger => {
  return bunyan.createLogger({
    level: opts.level,
    name: 'fab-ppl-directory',
    serializers: bunyan.stdSerializers
  });
};
