import {AxiosResponse, AxiosError, AxiosRequestConfig} from 'axios';
import {ASAPHandler} from '../lib/asap';
import {Interactor, interactorOptions} from './interactor';
import * as querystring from 'querystring';
import {iTraceInfo} from '../lib/tracer';

export class SessionNotFoundError extends Error {
  constructor(message: string, public originalError: AxiosError) {
    super(message);
    this.name = 'SessionNotFoundError';
  }
}

export class SessionExpiredError extends Error {
  constructor(message: string, public originalError: AxiosError) {
    super(message);
    this.name = 'SessionExpiredError';
  }
}

export class InvalidTokenError extends Error {
  constructor(message: string, public originalError: AxiosError) {
    super(message);
    this.name = 'InvalidTokenError';
  }
}

export interface SessionServiceOptions extends interactorOptions {};

export class SessionServiceInteractor extends Interactor {

  constructor(opts: SessionServiceOptions, asapHandler?: ASAPHandler) {
    super(opts, asapHandler);
  }

  async validate(sessionToken: string, traceInfo?: iTraceInfo): Promise<string> {

    const opts:AxiosRequestConfig = {
      timeout: 5000
    };

    if (traceInfo) {
      opts.headers = {
        'x-b3-traceid': traceInfo.traceId,
        'x-b3-spanid': traceInfo.spanId
      };
      if (traceInfo.parentSpanId) {
        opts.headers['x-b3-parentspanid'] = traceInfo.parentSpanId;
      }
    }
    return this._con
      .post('/validate', querystring.stringify({sessionToken}), opts)
      .then((res: AxiosResponse) => (res.data as string))
      .catch((err: AxiosError) => {
        throw this._errorMapper(err);
      });
  }

  async sessions(aaId: string) {
    return this._con
      .post('/sessions', querystring.stringify({aaId}))
      .then((res: AxiosResponse) => res.data)
      .catch((err: AxiosError) => {
        throw this._errorMapper(err);
      });
  }

  private _errorMapper(err: AxiosError): Error {
    if (!err.response || !err.response.data) {
      return err;
    }

    switch(err.response.data.error.key) {
      // httpstatus 400
      case 'validateSession.missingToken':
      case 'validateSession.invalidParam':
      case 'token.decode.error':
        return new InvalidTokenError(err.response.data.error.message, err);

      // httpstatus 401
      case 'validateSession.sessionNotFound':
        return new SessionNotFoundError(err.response.data.error.message, err);
      case 'validateSession.sessionExpired':
        return new SessionExpiredError(err.response.data.error.message, err);

      // everything else
      default:
        return err;
    }
  }
}
