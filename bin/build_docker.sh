#!/bin/bash
set -e
BASEDIR=$(dirname "$0")
BASENAME="pf-node-ref-service"
GITREV="$(git rev-parse --short=10 HEAD)"
IMAGE="${BASENAME}:${GITREV}";
echo "Building image ${IMAGE}"

docker build --rm -t docker.atlassian.io/${IMAGE} $BASEDIR/../
docker push docker.atlassian.io/${IMAGE}
